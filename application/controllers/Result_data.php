<?php

/**
 *
 */
class Result_data extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model('input_peserta');
  }

  public function index()
  {
    $data['result'] = $this->input_peserta->get_full_data();
    $data['view_file_path'] = 'result_data.php';
    $this->load->view('defaults/layout', $data);
  }
}
