<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Data extends CI_Controller {
        function __construct()
        {
            parent::__construct();
            $this->load->model('input_peserta');
        }
        public function index()
        {
            $data['result'] = $this->input_peserta->get();
            $data['view_file_path'] = 'input.php';
            $this->load->view('defaults/layout', $data);
        }

        public function input()
        {
            $this->form_validation->set_rules($this->config->item('input_data'));

            if ($this->form_validation->run() !== TRUE) {
                $this->session->set_flashdata('error', alert('gagal', $this->form_validation->error_string()));
                // $this->session->set_flashdata('error', '<div id="simpel">aslkdfjlasdjfklsadjf</div>');
                redirect($_SERVER['HTTP_REFERER']);
            }

            $result = $this->input_peserta->simpan($_POST);

            if ($result === TRUE) {
                $this->session->set_flashdata('success', alert('berhasil', 'data berhasil disimpan'));
            } else {
                $this->session->set_flashdata('error', alert('gagal', 'data gagal disimpan'));
            }

            redirect($_SERVER['HTTP_REFERER']);
        }

        public function edit()
        {
          $result = $this->input_peserta->edit($_POST);

          if ($result === TRUE) {
              $this->session->set_flashdata('success', alert('berhasil', 'data berhasil diedit'));
          } else {
              $this->session->set_flashdata('error', alert('gagal', 'data gagal diedit'));
          }

          redirect($_SERVER['HTTP_REFERER']);
        }

        public function hapus()
        {
          $result = $this->input_peserta->hapus($_POST);

          if ($result === TRUE) {
              $this->session->set_flashdata('success', alert('berhasil', 'data berhasil dihapus'));
          } else {
              $this->session->set_flashdata('error', alert('gagal', 'data gagal dihapus'));
          }

          redirect($_SERVER['HTTP_REFERER']);

        }

    }
