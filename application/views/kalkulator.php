<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section> -->

  <section class="content">
  <div class="box">
    <div class="box-header with-border">
      <h3>Aritmatika</h3>
    </div>
    <div class="box-body">

      <!-- Button to Open the Modal -->

      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalPenjumlahan">
          Penjumlahan
      </button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalPengurangan">
          Pengurangan
      </button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalPerkalian">
          Perkalian
      </button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalPembagian">
          Pembagian
      </button>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
    </div>
  </div>
    </section>
</div>


<!-- Modal Penjumlahan -->
<div class="modal fade modal-default" id="myModalPenjumlahan">
    <div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Penjumlahan</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        <form>
            <div class="form-group">
                <label for="nilai 1" class="form-control-label">Angka Pertama : </label>
                <input type="text" class="form-control" id="angka1-tambah">
            </div>
            <div class="form-group">
                <label for="nilai 2" class="form-control-label">Angka Kedua : </label>
                <input type="text" class="form-control" id="angka2-tambah">
            </div>
            <div class="form-group">
                <label for="" class="form-control-label">Hasil :</label>
                <input type="text" class="form-control" id="hasil-tambah" disabled><br>
                <input type="text" class="form-control" id="terbilang-tambah" disabled>
            </div>
        </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-info pull-left" id="play-tambah">Play</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
    </div>
</div>

    <!-- Modal Pengurangan -->
    <div class="modal fade modal-default" id="myModalPengurangan">
    <div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Pengurangan</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="nilai 1" class="form-control-label">Angka Pertama : </label>
                    <input type="text" class="form-control" id="angka1-kurang">
                </div>
                <div class="form-group">
                    <label for="nilai 2" class="form-control-label">Angka Kedua : </label>
                    <input type="text" class="form-control" id="angka2-kurang">
                </div>
                <div class="form-group">
                    <label for="" class="form-control-label">Hasil :</label>
                    <input type="text" class="form-control" id="hasil-kurang" disabled><br>
                    <input type="text" class="form-control" id="terbilang-kurang" disabled>
                </div>
            </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-info" id="play-kurang">Play</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
    </div>
</div>

    <!-- Modal Perkalian -->
    <div class="modal fade modal-default" id="myModalPerkalian">
    <div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Perkalian</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
            <form>
                <div class="form-group">
                    <label for="nilai 1" class="form-control-label">Angka Pertama : </label>
                    <input type="text" class="form-control" id="angka1-kali">
                </div>
                <div class="form-group">
                    <label for="nilai 2" class="form-control-label">Angka Kedua : </label>
                    <input type="text" class="form-control" id="angka2-kali">
                </div>
                <div class="form-group">
                    <label for="" class="form-control-label">Hasil :</label>
                    <input type="text" class="form-control" id="hasil-kali" disabled><br>
                    <input type="text" class="form-control" id="terbilang-kali" disabled>
                </div>
            </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-info" id="play-kali">Play</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
    </div>
</div>

    <!-- Modal Pembagian -->
    <div class="modal fade modal-default" id="myModalPembagian">
    <div class="modal-dialog">
    <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
        <h4 class="modal-title">Pembagian</h4>
        <button type="button" class="close" data-dismiss="modal">×</button>
        </div>

        <!-- Modal body -->
        <div class="modal-body">
        <form>
                <div class="form-group">
                    <label for="nilai 1" class="form-control-label">Angka Pertama : </label>
                    <input type="text" class="form-control" id="angka1-bagi">
                </div>
                <div class="form-group">
                    <label for="nilai 2" class="form-control-label">Angka Kedua : </label>
                    <input type="text" class="form-control" id="angka2-bagi">
                </div>
                <div class="form-group">
                    <label for="" class="form-control-label">Hasil :</label>
                    <input type="text" class="form-control" id="hasil-bagi" disabled><br>
                    <input type="text" class="form-control" id="terbilang-bagi" disabled>
                </div>
            </form>
        </div>

        <!-- Modal footer -->
        <div class="modal-footer">
        <button type="button" class="btn btn-info" id="play-bagi">Play</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>

    </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        //menangkap action dari user dan dimasukan kevariabel
        $('#angka1-tambah, #angka2-tambah').keyup(function(){
            var angka1 = $('#angka1-tambah').val();
            var angka2 = $('#angka2-tambah').val();

            // jika variabel null atau kosong diisi dengan nilai 0

            if (!angka1 && !angka2 ) {
                angka1 = 0;
                angka2 = 0;
            } else if (! angka1) {
                angka1 = 0;
            } else if (! angka2) {
                angka2 = 0;
            }

            //menampilkah hasil penjumlahan
            var hasilTambah = parseInt(angka1) + parseInt(angka2);
            $('#hasil-tambah').val(hasilTambah);

            // fungsi konversi nilai ke jumlah terbilang
            var tambah = terbilang(hasilTambah);
            $('#terbilang-tambah').val(tambah);

            $('#play-tambah').click(function(){
            responsiveVoice.speak(tambah, "Indonesian Male",{
                pitch : 1,
                rate : 1,
                volume: 2
                });
            });
        });



        $('#angka1-kurang, #angka2-kurang').keyup(function(){
            var angka1 = $('#angka1-kurang').val();
            var angka2 = $('#angka2-kurang').val();

            if (!angka1 && !angka2 ) {
                angka1 = 0;
                angka2 = 0;
            } else if (! angka1) {
                angka1 = 0;
            } else if (! angka2) {
                angka2 = 0;
            }

            var hasilKurang = parseInt(angka1) - parseInt(angka2);
            $('#hasil-kurang').val(hasilKurang);

            var kurang = terbilang(hasilKurang);
            $('#terbilang-kurang').val(kurang);

            $('#play-kurang').click(function(){
            responsiveVoice.speak(kurang, "Indonesian Male",{
                pitch : 1,
                rate : 1,
                volume: 2
                });
            });
        });

        $('#angka1-kali, #angka2-kali').keyup(function(){
            var angka1 = $('#angka1-kali').val();
            var angka2 = $('#angka2-kali').val();

            if (!angka1 && !angka2 ) {
                angka1 = 0;
                angka2 = 0;
            } else if (! angka1) {
                angka1 = 0;
            } else if (! angka2) {
                angka2 = 0;
            }

            var hasilKali = parseInt(angka1) * parseInt(angka2);
            $('#hasil-kali').val(hasilKali);

            var kali = terbilang(hasilKali);
            $('#terbilang-kali').val(kali);

            $('#play-kali').click(function(){
            responsiveVoice.speak(kali, "Indonesian Male",{
                pitch : 1,
                rate : 1,
                volume: 2
                });
            });
        });

        $('#angka1-bagi, #angka2-bagi').keyup(function(){
            var angka1 = $('#angka1-bagi').val();
            var angka2 = $('#angka2-bagi').val();

            if (!angka1 && !angka2 ) {
                angka1 = 0;
                angka2 = 0;
            } else if (! angka1) {
                angka1 = 0;
            } else if (! angka2) {
                angka2 = 0;
            }

            var hasilBagi = parseInt(angka1) / parseInt(angka2);
            var convertBagi = Math.round(hasilBagi);
            $('#hasil-bagi').val(convertBagi);

            var bagi = terbilang(convertBagi);
            $('#terbilang-bagi').val(bagi);

            $('#play-bagi').click(function(){
            responsiveVoice.speak(bagi, "Indonesian Male",{
                pitch : 1,
                rate : 1,
                volume: 2
                });
            });
        });
    });
</script>
