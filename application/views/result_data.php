<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <!-- <section class="content-header">
    <ol class="breadcrumb">
        <i class="fa fa-arrows"></i>&nbsp;
    </ol>
  </section> -->

  <!-- Main content -->
  <section class="content">
  <?php
    if (! empty($this->session->flashdata('success'))) {
      echo $this->session->flashdata('success');
    }

    if (! empty($this->session->flashdata('error'))) {
      echo $this->session->flashdata('error');
    }
  ?>
    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3>Data Peserta Lengkap</h3>
      </div>
      <div class="box-body">
        <table class="table">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>NIK</th>
                    <th>HP</th>
                    <th>E-Mail</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            <tbody>
            <?php if (! empty($result)) :?>
              <?php $no = 1 ?>
                <?php foreach($result as $row) : ?>
                  <tr>
                      <td><?php echo $no++ ?></td>
                      <td><?php echo $row['nama'] ?></td>
                      <td><?php echo $row['NIK'] ?></td>
                      <td><?php echo $row['HP'] ?></td>
                      <td><?php echo $row['email'] ?></td>
                      <td align="center">
                        <button class="btn-lihat-data btn btn-info btn-xs" title="Lihat Pengguna"
                        data-nama = "<?php echo $row['nama'] ?>";
                        data-nik = "<?php echo $row['NIK'] ?>";
                        data-hp = "<?php echo $row['HP'] ?>";
                        data-email = "<?php echo $row['email'] ?>";
                        data-skema = "<?php echo $row['skema'] ?>";
                        data-rekomendasi = "<?php echo $row['rekomendasi'] ?>";
                        data-tglterbit = "<?php echo $row['tanggal_terbit'] ?>";
                        data-tgllahir = "<?php echo $row['tanggal_lahir'] ?>";
                        data-organisasi = "<?php echo $row['organisasi'] ?>";
                          data-toggle="modal" data-target="#mdl-lihat">
                              <i class="fa fa-eye"></i>
                        </button>

                        <button class="btn-ubah-data btn btn-warning btn-xs" title="Edit Pengguna"
                          data-id = "<?php echo $row['id'] ?>";
                          data-nama = "<?php echo $row['nama'] ?>";
                          data-nik = "<?php echo $row['NIK'] ?>";
                          data-hp = "<?php echo $row['HP'] ?>";
                          data-email = "<?php echo $row['email'] ?>";
                          data-skema = "<?php echo $row['skema'] ?>";
                          data-rekomendasi = "<?php echo $row['rekomendasi'] ?>";
                          data-tglterbit = "<?php echo $row['tanggal_terbit'] ?>";
                          data-tgllahir = "<?php echo $row['tanggal_lahir'] ?>";
                          data-organisasi = "<?php echo $row['organisasi'] ?>";
                          data-toggle="modal" data-target="#mdl-edit">
                              <i class="fa fa-edit"></i>
                        </button>

                        <button class="btn-hapus-data btn btn-danger btn-xs" title="Hapus Pengguna"
                                    data-id = "<?php echo $row['id'] ?>"
                                    data-nama = "<?php echo $row['nama'] ?>"
                                    data-toggle="modal" data-target="#mdl-delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                      </td>
                  </tr>
                <?php endforeach ?>
            <?php endif ?>
            </tbody>
        </table>
      </div>
      <!-- /.box-footer-->
    </div>
    <!-- /.box -->

  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<!-- Modals Lihat Data -->
  <div class="modal fade modal-default" id="mdl-lihat">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Lihat Data</h4>
        </div>
        <?php echo form_open('Data/edit');?>
        <div class="modal-body">
          <div class="form-group row">
            <input type="hidden" id="ed-id" name="ed_id">
            <label for="#" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-nama" name="ed_nama" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-nik" name="ed_nik" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-hp" name="ed_hp" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">E-Mail</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-mail" name="ed_mail" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-skema" name="ed_skema" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-rekomendasi" name="ed_rekomendasi" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="eye-tgl-terbit" name="ed_tgl_terbit" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="eye-tgl-lahir" name="ed_tgl_lahir" disabled>
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Organisasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="eye-organisasi" name="ed_organisasi" disabled>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
<!-- // Modals -->


<!-- Modals Tambah Data -->
  <div class="modal fade modal-default" id="mdl-edit">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title"><i class="fa fa-group"></i>&nbsp;&nbsp;Input</h4>
        </div>
        <?php echo form_open('Data/edit');?>
        <div class="modal-body">
          <div class="form-group row">
            <input type="hidden" id="ed-id" name="ed_id">
            <label for="#" class="col-sm-2 control-label">Nama</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-nama" name="ed_nama" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">NIK</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-nik" name="ed_nik" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">HP</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-hp" name="ed_hp" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">E-Mail</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-mail" name="ed_mail" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Skema Sertifikasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-skema" name="ed_skema" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Rekomendasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-rekomendasi" name="ed_rekomendasi" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Terbit</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="ed-tgl-terbit" name="ed_tgl_terbit" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Tanggal Lahir</label>
            <div class="col-sm-10">
              <input type="text" class="form-control datepicker" id="ed-tgl-lahir" name="ed_tgl_lahir" placeholder="#">
            </div>
          </div>
          <div class="form-group row">
            <label for="#" class="col-sm-2 control-label">Organisasi</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="ed-organisasi" name="ed_organisasi" placeholder="#">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
          <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
        </div>
        <?php echo form_close();?>
      </div>
    </div>
  </div>
<!-- // Modals -->

  <!-- Modals Hapus data -->
  <?php echo form_open('Data/hapus') ?>
    <div class="modal fade modal-default" id="mdl-delete">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title"><i class="fa fa-user"></i>&nbsp;&nbsp;Hapus Pengguna</h4>
          </div>

          <div class="modal-body">
            <div class="form-group row">
              <input type="hidden" name="del_id" id="del-id">

              <div class="col-sm-12">
                Hapus pengguna <span class="label label-danger label-lg" id="del-nama"></span> ??
              </div>

            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;&nbsp;Hapus</button>
          </div>
        </div>
      </div>
    </div>
  <?php echo form_close() ?>

  <script type="text/javascript">
    $('.btn-lihat-data').click(function(){
      var nama = $(this).data('nama');
      var nik  = $(this).data('nik');
      var hp   = $(this).data('hp');
      var email = $(this).data('email');
      var skema = $(this).data('skema');
      var rekomendasi = $(this).data('rekomendasi');
      var tanggal_terbit = $(this).data('tglterbit');
      var tanggal_lahir = $(this).data('tgllahir');
      var organisasi = $(this).data('organisasi');

      $('#eye-nama').val(nama);
      $('#eye-nik').val(nik);
      $('#eye-hp').val(hp);
      $('#eye-mail').val(email);
      $('#eye-skema').val(skema);
      $('#eye-rekomendasi').val(rekomendasi);
      $('#eye-tgl-terbit').val(tanggal_terbit);
      $('#eye-tgl-lahir').val(tanggal_lahir);
      $('#eye-organisasi').val(organisasi);
    });

    $('.btn-ubah-data').click(function(){
      var id   = $(this).data('id');
      var nama = $(this).data('nama');
      var nik  = $(this).data('nik');
      var hp   = $(this).data('hp');
      var email = $(this).data('email');
      var skema = $(this).data('skema');
      var rekomendasi = $(this).data('rekomendasi');
      var tanggal_terbit = $(this).data('tglterbit');
      var tanggal_lahir = $(this).data('tgllahir');
      var organisasi = $(this).data('organisasi');

      $('#ed-id').val(id);
      $('#ed-nama').val(nama);
      $('#ed-nik').val(nik);
      $('#ed-hp').val(hp);
      $('#ed-mail').val(email);
      $('#ed-skema').val(skema);
      $('#ed-rekomendasi').val(rekomendasi);
      $('#ed-tgl-terbit').val(tanggal_terbit);
      $('#ed-tgl-lahir').val(tanggal_lahir);
      $('#ed-organisasi').val(organisasi);
    });

    $('.btn-hapus-data').click(function(){
      var id = $(this).data('id');
      var nama = $(this).data('nama');

      $('#del-id').val(id);
      $('#del-nama').text(nama);
    });
  </script>
