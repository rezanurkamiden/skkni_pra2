<?php
    defined('BASEPATH') or exit('Direct access script is not allowed');

    class Input_peserta extends CI_Model {

        public function simpan()
        {
            $data = array(
                'nama'  => $_POST['in_nama'],
                'NIK'   => $_POST['in_nik'],
                'HP'    => $_POST['in_hp'],
                'email' => $_POST['in_mail'],
                'skema' => $_POST['in_skema'],
                'rekomendasi' => $_POST['in_rekomendasi'],
                'tanggal_terbit' => $_POST['in_tgl_terbit'],
                'tanggal_lahir' => $_POST['in_tgl_lahir'],
                'organisasi' => $_POST['in_organisasi']
            );

            $this->db->insert('data', $data);

            if ($this->db->affected_rows() > 0) {
                return TRUE;
            } else {
                return FALSE;
            }
        }

        public function get()
        {
            $sql = "CALL cetak_data_dua();";

            return $this->db->query($sql)->result_array();
        }

        public function edit() {

          $data = array(
              'nama'  => $_POST['ed_nama'],
              'NIK'   => $_POST['ed_nik'],
              'HP'    => $_POST['ed_hp'],
              'email' => $_POST['ed_mail'],
              'skema' => $_POST['ed_skema'],
              'rekomendasi' => $_POST['ed_rekomendasi'],
              'tanggal_terbit' => $_POST['ed_tgl_terbit'],
              'tanggal_lahir' => $_POST['ed_tgl_lahir'],
              'organisasi' => $_POST['ed_organisasi']
          );
          $id = $_POST['ed_id'];

          $this->db->set($data);
          $this->db->where('id', $id);
          $this->db->update('data');

          if ($this->db->affected_rows() > 0) {
            return TRUE;
          } else {
            return FALSE;
          }
        }

        public function hapus()
        {
          $id = $_POST['del_id'];

          $this->db->where('id', $id);
          $this->db->delete('data');

          if ($this->db->affected_rows() > 0) {
            return TRUE;
          } else {
            return FALSE;
          }
        }

        public function get_full_data()
        {
          $sql = 'SELECT * FROM data';

          return $this->db->query($sql)->result_array();
        }

    }
