<?php

//form validation config

$config['input_data'] = array(
    array(
        'field' => 'in_nama',
        'label' => 'Nama Peserta',
        'rules' => 'required|max_length[255]',
        'errors' => array(
            'required' =>  '%s harus diisi',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' =>  'in_nik',
        'label' =>  'NIK',
        'rules' =>  'required|is_natural|max_length[100]',
        'errors' => array(
            'required'  => '%s harus diisi',
            'is_natural'    =>  'Format %s salah',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' => 'in_hp',
        'label' => 'HP',
        'rules' =>  'required|is_natural|max_length[100]',
        'errors' => array(
            'required' => '%s harus diisi',
            'is_natural' => 'format %s salah',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' => 'in_mail',
        'label' => 'EMAIL',
        'rules' => 'required|valid_email|max_length[100]',
        'errors' => array(
            'required' => '%s harus diisi',
            'valid_email' => 'Format %s salah',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' => 'in_skema',
        'label' => 'Skema',
        'rules' => 'required|max_length[100]',
        'errors' => array(
            'required' => '%s harus disi',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' => 'in_rekomendasi',
        'label' => 'Rekomendasi',
        'rules' => 'required|max_length[100]',
        'errors' => array(
            'required' => '%s harus diisi',
            'max_length' => '%s terlalu panjang'
        ),
    ),

    array(
        'field' => 'in_tgl_terbit',
        'label' => 'Tanggal Terbit',
        'rules' => 'required',
        'errors' => array(
            'required' => '%s harus diisi'
        ),
    ),

    array(
        'field' => 'in_tgl_lahir',
        'label' => 'Tanggal Lahir',
        'rules' => 'required',
        'errors' => array(
            'required' => '%s harus diisi'
        ),
    ),

    array(
        'field' => 'in_organisasi',
        'label' => 'Organisasi',
        'rules' => 'required|max_length[100]',
        'errors' => array(
            'required' => '%s harus diisi',
            'max_length' => '% terlalu panjang'
        ),
    ),
);